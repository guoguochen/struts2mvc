package mvc.util;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class DispatchFilter implements Filter {
	private Map<String, XmlBean> configmap;

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		String path = "login";// getpath(request.getServletPath());
		XmlBean xmlb = configmap.get(path);

		try {
			Class actionclass = Class.forName(xmlb.getActionclass());
			Object action = actionclass.newInstance();
			Method actionMethod = actionclass.getDeclaredMethod(xmlb
					.getMethodname());
			

			Map<String, String[]> paramap = request.getParameterMap();
			Field[] f = actionclass.getDeclaredFields();

			Method setter;
			for (Field f0 : f) {// 根据action中的字段遍历
				if (paramap.get(f0.getName()) != null) {// 如果字段有参数中有值
					setter = actionclass.getMethod("set"
							+ f0.getName().substring(0, 1).toUpperCase()
							+ f0.getName().substring(1),f0.getType());
					setter.invoke(action, paramap.get(f0.getName()));
				}
			}
			String resultname = (String) actionMethod.invoke(action);
			String url = xmlb.getResult().get(resultname);
			request.getRequestDispatcher(url).forward(request, response);
			//chain.doFilter(request, response);
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (InstantiationException e1) {
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			e1.printStackTrace();
		} catch (SecurityException e1) {
			e1.printStackTrace();
		} catch (NoSuchMethodException e1) {
			e1.printStackTrace();
		} catch (IllegalArgumentException e1) {
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			e1.printStackTrace();
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// 加载
		ServletContext context = arg0.getServletContext();
		String realpath = "WEB-INF\\classes\\struts.xml";// context.getInitParameter("struts");
		String tomcatepath = context.getRealPath("\\");
		configmap = ConfigParse.parse(tomcatepath + realpath);

		System.out.println("信息：系统加载完成");
	}
}
