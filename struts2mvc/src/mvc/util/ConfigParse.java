package mvc.util;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

public class ConfigParse {
	static Map<String, XmlBean> parse(String xmlpath) {
		Map<String, XmlBean> configmap = new HashMap<String, XmlBean>();
		SAXBuilder saxb = new SAXBuilder();
		try {
			Document doc = saxb.build(new File(xmlpath));
			Element root = doc.getRootElement();

			// Element actionmappings = root.getChild("struts");
			List<Element> actions = root.getChildren();
			for (Element action : actions) {
				XmlBean xmlb = new XmlBean();
				String actionname = action.getAttributeValue("name");
				xmlb.setName(actionname);
				String actionclass = action.getAttributeValue("class");
				xmlb.setActionclass(actionclass);
				String methodname = action.getAttributeValue("method");
				if (methodname == null)
					methodname = "execute";
				xmlb.setMethodname(methodname);

				List<Element> forwards = action.getChildren();
				Map<String, String> mappingforward = new HashMap<String, String>();
				for (Element forward : forwards) {
					mappingforward.put(forward.getAttributeValue("name"),
							forward.getText());
				}
				xmlb.setResult(mappingforward);
				configmap.put(actionname, xmlb);
			}
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return configmap;
	}
}
