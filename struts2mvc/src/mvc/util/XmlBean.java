package mvc.util;

import java.util.Map;

public class XmlBean {
	private String name;
	private String actionclass;
	private String methodname;
	private Map<String, String> result;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "XmlBean [name=" + name + ", actionclass=" + actionclass
				+ ", methodname=" + methodname + ", result=" + result + "]";
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the actionclass
	 */
	public String getActionclass() {
		return actionclass;
	}

	/**
	 * @param actionclass
	 *            the actionclass to set
	 */
	public void setActionclass(String actionclass) {
		this.actionclass = actionclass;
	}

	/**
	 * @return the methodname
	 */
	public String getMethodname() {
		return methodname;
	}

	/**
	 * @param methodname
	 *            the methodname to set
	 */
	public void setMethodname(String methodname) {
		this.methodname = methodname;
	}

	/**
	 * @return the result
	 */
	public Map<String, String> getResult() {
		return result;
	}

	/**
	 * @param result
	 *            the result to set
	 */
	public void setResult(Map<String, String> result) {
		this.result = result;
	}

}
