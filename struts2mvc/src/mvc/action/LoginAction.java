package mvc.action;

import java.util.List;

public class LoginAction {
	private String username;
	private String password;
	private User user;
	private List users;

	/**
	 * @return the users
	 */
	public List getUsers() {
		return users;
	}

	/**
	 * @param users
	 *            the users to set
	 */
	public void setUsers(List users) {
		this.users = users;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	public String execute() {
		User newuser = new User(getUsername(), getPassword());
		UserService userserv = new UserService();
		User u = userserv.getUser(newuser);
		if (u != null)
			return "success";
		else
			return "error";
	}

	public String registForm() {
		UserService userserv = new UserService();
		boolean b = userserv.save(getUser());
		if (b)
			return "success";
		else
			return "error";
	}

	public String list() {
		UserService userserv = new UserService();
		users = userserv.list();
		return "success";
	}
}
