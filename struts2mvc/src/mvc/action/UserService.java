package mvc.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class UserService {
	private static List<User> users;
	static {
		users = new ArrayList<User>();
		users.add(new User("chenli", "123", 41.2, new Date()));
		users.add(new User("chenli2", "1232", 42.2, new Date()));
		users.add(new User("chenli3", "1233", 43.2, new Date()));
		users.add(new User("chenli4", "1234", 44.2, new Date()));
	}

	public User getUser(User user) {
		for (User u : users) {
			if (u.getUsername().equalsIgnoreCase(user.getUsername())
					&& u.getPassword().equalsIgnoreCase(user.getPassword()))
				return u;
		}
		return null;
	}

	public boolean save(User user) {
		for (User u : users) {
			if (u.getUsername().equalsIgnoreCase(user.getUsername())
					&& u.getPassword().equalsIgnoreCase(user.getPassword()))
				return false;
		}
		users.add(user);
		return true;
	}

	public List list() {
		return users;
	}
}
